# tauri-update-server

[![](https://shields.kaki87.net/endpoint?url=https%3A%2F%2Fraw.githubusercontent.com%2Fserver-KaTys%2Fstatus%2Fmaster%2Fapi%2Fka-ki87-tauri-updates%2Fuptime.json)](https://status.katys.eu.org/history/ka-ki87-tauri-updates)

Automatically interface [Tauri's updater](https://tauri.app/fr/v1/guides/distribution/updater/) with your git repository's releases.

## Usage

### Self-host the server (optional)

```bash
cp config.example.js config.js # TODO: fill
deno run --allow-net --allow-read=data --allow-write=data main.js
```

### Add/remove repos

```bash
deno run --allow-net https://git.kaki87.net/KaKi87/tauri-update-server/raw/branch/master/cli.js
```

When self-hosting, specify your server URL with `--url=<url>` (`https://tauri-updates.kaki87.net` by default).

In order to verify repo ownership, you will have to push a commit with a message ending with a specific code.
You may use empty commits :

```bash
git commit --allow-empty -m "Enable/disable updater <code>"
```

### Configure apps

In `tauri.conf.json` :

```json
{
    "tauri": {
        "updater": {
            "active": true,
            "endpoints": [
                "<url>/repo/<id>/update/{{target}}/{{current_version}}"
            ]
        }
    }
}
```

### Publish updates

In your releases, include the following files :
- `*_amd64.AppImage.tar.gz` & `*_amd64.AppImage.tar.gz.sig` for Linux
- `*.msi.zip` & `*.msi.zip.sig` for Windows
- `*.app.tar.gz` & `*.app.tar.gz.sig` for Mac
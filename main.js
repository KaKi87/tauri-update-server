import axios from 'npm:axios';
import pogo from 'https://deno.land/x/pogo@v0.5.2/main.ts';
import {
    createServerHelper
} from 'https://git.kaki87.net/KaKi87/pogo-helpers/raw/commit/c09bb256d310dad935c9406c322a896b7898fa66/mod.js';
import {
    createDatastore
} from 'https://git.kaki87.net/KaKi87/xedb/raw/commit/913a894ef5e4e96bc885e57f8e9bb4d5fc97bf6f/mod.js';
import deno from 'https://git.kaki87.net/KaKi87/xedb/raw/commit/913a894ef5e4e96bc885e57f8e9bb4d5fc97bf6f/lib/deno.js';
import Joi from 'npm:joi@17.6.1';
import * as semver from 'https://deno.land/std@0.161.0/semver/mod.ts';
import {
    nanoid
} from 'https://cdn.jsdelivr.net/gh/ai/nanoid@386e4f921cf95d390266e83b04e35c2e1294db67/index.browser.js';

import {
    port
} from './config.js';

const
    server = pogo.server({
        port
    }),
    {
        createRoute
    } = createServerHelper(
        server,
        {
            isCors: true,
            onError: error => console.error(error)
        }
    ),
    repos = createDatastore({
        path: './data/repos.ndjson',
        runtime: deno
    });

createRoute({
    method: 'POST',
    path: '/repo',
    schema: Joi.object({
        path: Joi.object(),
        params: Joi.object(),
        body: Joi.object({
            owner: Joi.string().required(),
            name: Joi.string().required(),
            host: Joi.string().required().domain()
        })
    }),
    handler: async (
        request,
        h,
        {
            body,
            response
        }
    ) => {
        const { _id } = await repos.insertOne(body);
        response.body = { id: _id };
        response.code(201);
        return response;
    }
});

createRoute({
    method: 'PATCH',
    path: '/repo/{id}/activate',
    schema: Joi.object({
        path: Joi.object({
            id: Joi.string().required()
        }),
        params: Joi.object({
            hash: Joi.string().required()
        })
    }),
    handler: async (
        request,
        h,
        {
            path,
            params,
            response
        }
    ) => {
        const
            {
                id
            } = path,
            {
                hash
            } = params,
            {
                host,
                owner,
                name
            } = await repos.findOne({
                _id: id
            }) || {};
        if(
            host && owner && name
            &&
            (await axios({
                baseURL: `https://${host}/api/v1`,
                url: `/repos/${owner}/${name}/git/commits/${hash}`,
                validateStatus: status => [200, 404].includes(status)
            })).data['commit']?.['message']?.trim()?.endsWith(id)
        ){
            await repos.updateOne(
                { _id: id},
                {
                    $set: {
                        isActive: true
                    }
                }
            );
            response.code(204);
        }
        else
            response.code(400);
        return response;
    }
});

{
    const handler = async (
        request,
        h,
        {
            path,
            response
        }
    ) => {
        let
            {
                id,
                owner,
                name
            } = path,
            host,
            isActive;
        ({
            _id: id,
            owner,
            name,
            host,
            isActive
        } = await repos.findOne({
            ...id ? { _id: id } : {},
            ...owner && name ? { owner, name } : {}
        }) || {});
        if(id && owner && name){
            response.body = {
                id,
                owner,
                name,
                host,
                isActive
            };
        }
        else
            response.code(404);
        return response;
    };
    createRoute({
        method: 'GET',
        path: '/repo/{id}',
        schema: Joi.object({
            path: Joi.object({
                id: Joi.string().required()
            }),
            params: Joi.object()
        }),
        handler
    });
    createRoute({
        method: 'GET',
        path: '/repo/{owner}/{name}',
        schema: Joi.object({
            path: Joi.object({
                owner: Joi.string().required(),
                name: Joi.string().required()
            }),
            params: Joi.object()
        }),
        handler
    });
}

{
    const
        pathSchema = Joi.object({
            target: Joi.string().required().valid('linux', 'windows', 'darwin'),
            currentVersion: Joi.string().required().custom((value, helpers) => semver.valid(value) ? value : helpers.error('any.invalid'))
        }),
        handler = async (
            request,
            h,
            {
                path,
                response
            }
        ) => {
            let
                {
                    id,
                    owner,
                    name,
                    target,
                    currentVersion
                } = path,
                host,
                isActive;
            ({
                _id: id,
                owner,
                name,
                host,
                isActive
            } = await repos.findOne({
                ...id ? { _id: id } : {},
                ...owner && name ? { owner, name } : {}
            }) || {});
            if(host){
                if(isActive){
                    const
                        assetNameSuffix = {
                            'linux': '_amd64.AppImage.tar.gz',
                            'windows': '.msi.zip',
                            'darwin': '.app.tar.gz'
                        }[target],
                        getSignatureAsset = release => release['assets'].find(asset => asset.name.endsWith(`${assetNameSuffix}.sig`)),
                        getTargetAsset = release => release['assets'].find(asset => asset.name.endsWith(assetNameSuffix)),
                        release = (await axios({
                            baseURL: `https://${host}/api/v1`,
                            url: `/repos/${owner}/${name}/releases`
                        })).data.find(release =>
                            semver.gt(release['tag_name'], currentVersion)
                            &&
                            getSignatureAsset(release)
                            &&
                            getTargetAsset(release)
                        );
                    if(release){
                        response.body = {
                            'url': getTargetAsset(release)['browser_download_url'],
                            'version': release['tag_name'],
                            'notes': release['body'],
                            'pub_date': release['published_at'],
                            'signature': (await axios(getSignatureAsset(release)['browser_download_url'])).data
                        };
                    }
                    else
                        response.code(204);
                }
                else
                    response.code(400);
            }
            else
                response.code(404);
            return response;
        };
    createRoute({
        method: 'GET',
        path: '/repo/{id}/update/{target}/{currentVersion}',
        schema: Joi.object({
            path: pathSchema.keys({
                id: Joi.string().required()
            }),
            params: Joi.object()
        }),
        handler
    });
    createRoute({
        method: 'GET',
        path: '/repo/{owner}/{name}/update/{target}/{currentVersion}',
        schema: Joi.object({
            path: pathSchema.keys({
                owner: Joi.string().required(),
                name: Joi.string().required()
            }),
            params: Joi.object()
        }),
        handler
    });
}

createRoute({
    method: 'DELETE',
    path: '/repo/{id}',
    schema: Joi.object({
        path: Joi.object({
            id: Joi.string().required()
        }),
        params: Joi.object({
            hash: Joi.string()
        })
    }),
    handler: async (
        request,
        h,
        {
            path,
            params,
            response
        }
    ) => {
        const
            { id } = path,
            { hash } = params;
        let {
            host,
            owner,
            name,
            isActive,
            deletionCode
        } = await repos.findOne({ _id: id }) || {};
        if(host && owner && name){
            if(!isActive || hash){
                if(
                    !isActive
                    ||
                    (await axios({
                        baseURL: `https://${host}/api/v1`,
                        url: `/repos/${owner}/${name}/git/commits/${hash}`,
                        validateStatus: status => [200, 404].includes(status)
                    })).data['commit']?.['message']?.trim()?.endsWith(deletionCode)
                ){
                    await repos.deleteOne({ _id: id });
                    response.code(204);
                }
                else
                    response.code(400);
            }
            else {
                if(!deletionCode){
                    deletionCode = nanoid();
                    await repos.updateOne(
                        { _id: id },
                        {
                            $set: {
                                deletionCode
                            }
                        }
                    );
                }
                response.code(202);
                response.body = {
                    deletionCode
                };
            }
        }
        else
            response.code(404);
        return response;
    }
});

await repos.load();

server.start();
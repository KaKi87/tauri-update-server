import {
    parse as parseFlags
} from 'https://deno.land/std@0.119.0/flags/mod.ts';
import axios from 'npm:axios';
import outdent from 'https://deno.land/x/outdent@v0.8.0/mod.ts';
import AsciiTable from 'npm:ascii-table@0.0.9';

const
    {
        url
    } = parseFlags(
        Deno.args,
        {
            string: [
                'url'
            ],
            default: {
                url: 'https://tauri-updates.kaki87.net'
            }
        }
    ),
    client = axios.create({
        baseURL: url
    }),
    validateAction = async ({
        action,
        id,
        hash
    }) => {
        const {
            status
        } = await client({
            ...{
                'activate': {
                    method: 'PATCH',
                    url: `/repo/${id}/activate`
                },
                'delete': {
                    method: 'DELETE',
                    url: `/repo/${id}`
                }
            }[action],
            params: {
                hash
            },
            validateStatus: status => [204, 400].includes(status)
        });
        return status === 204;
    };

let
    id,
    owner,
    name,
    host,
    isActive;

id = prompt(outdent `
    To edit an existing repo, input its ID.
    To add a new one, press Enter.
    ID >
`)
if(id){
    let status;
    ({
        status,
        data: {
            owner,
            name,
            host,
            isActive
        } = {}
    } = await client(
        `/repo/${id}`,
        {
            validateStatus: status => [200, 404].includes(status)
        }
    ));
    if(status === 404)
        console.error('[!] Not found');
    else {
        const table = new AsciiTable();
        table.addRow('Owner', owner);
        table.addRow('Name', name);
        table.addRow('Host', host);
        table.addRow('Active', isActive ? 'Yes': 'No')
        switch(prompt(outdent `
            ${table}
            Choose an action.
            1. Activate
            2. Delete
            > 
        `)){
            case '1': {
                const
                    hash = prompt(outdent `
                        Push a commit which message ends with the repo ID.
                        Hash >
                    `);
                if(await validateAction({
                    action: 'activate',
                    id,
                    hash
                })) console.log('[✓] Success');
                else console.error('[!] Validation failed');
                break;
            }
            case '2': {
                const
                    {
                        status,
                        data: {
                            deletionCode
                        } = {}
                    } = await client.delete(
                        `/repo/${id}`,
                        {
                            validateStatus: status => [202, 204].includes(status)
                        }
                    );
                if(status === 202){
                    const hash = prompt(outdent `
                        Deletion code: ${deletionCode}
                        Push a commit which message ends with it.
                        Hash >
                    `);
                    if(await validateAction({
                        action: 'delete',
                        id,
                        hash
                    })) console.log('[✓] Success');
                    else console.error('[!] Validation failed');
                }
                else
                    console.log('[✓] Success');
                break;
            }
            default: {
                console.error('[!] Invalid input');
                break;
            }
        }
    }
}
else {
    owner = prompt('Owner >');
    name = prompt('Name >');
    host = prompt('Host >');
    ({
        data: {
            id
        }
    } = await client.post(
        '/repo',
        {
            owner,
            name,
            host
        }
    ));
    const
        hash = prompt(outdent `
            ID: ${id}
            Push a commit which message ends with it.
            Hash >
        `);
    if(await validateAction({
        action: 'activate',
        id,
        hash
    })) console.log('[✓] Success');
    else console.error('[!] Validation failed');
}
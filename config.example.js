export const
    /**
     * @type {number}
     * HTTP server listening port
     */
    port = undefined;